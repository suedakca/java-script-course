let markMass = 95;
let markHeight = 1.88;
let johnMass = 85;
let johsHeight = 1.76;

let markBMI = markMass / markHeight ** 2;
let johnBMI = johnMass / johsHeight ** 2;

let higherBMI = markBMI > johnBMI;
console.log(markBMI, johnBMI, higherBMI);

if (markBMI > johnBMI) {
    console.log(`Mark's BMI (${markBMI}) is higher than John's (${johnBMI})!`);
}
else {
    console.log(`John's BMI (${johnBMI}) is higher than Mark's (${markBMI})!`);
}