/*


let javaScriptIsFun = true;

console.log(typeof true);
console.log(typeof 23);
console.log(typeof 'jones');
console.log(typeof javaScriptIsFun);

javaScriptIsFun = 'YES!';

console.log(typeof javaScriptIsFun);

let year;
console.log(year);
console.log(typeof year);

year = 1991;

console.log(typeof year);

console.log(typeof null);


let country = "Turkey";
let population = "82M";

console.log(typeof country);
console.log(typeof population);
let isIsland = false;
let language;

console.log(typeof isIsland);
console.log(typeof language);


const language = 'Turkish';
let population = "82M";
console.log(language);
console.log(population);

population = '90M';
console.log(population);


// Math operators
const now = 2037;
const ageJonas = now - 1991;
const ageSarah = now - 2018;
console.log(ageJonas);
console.log(ageSarah);

console.log(ageJonas * 2, ageJonas / 4, ageSarah * 6, 2 ** 3);

let firstName = 'Sueda';
let secondName = "Akça";

console.log(firstName + ' ' + secondName);

// Assigment operators 
let x = 12 + 3;
console.log(x);
x += 10;
console.log(x);
x *= 4;
console.log(x);
x--;
console.log(x);


//Comparison operators
console.log(ageJonas > ageSarah); // <, >, <=, >=

console.log(ageSarah >= 18);

const isFullAge = ageSarah >= 18;


let country = "Turkey";
let population = 80;
let cont = "Asia";

console.log(population / 2);
population++;
console.log(population);
const language = 'Turkish';
let finlandPop = 6;
console.log(population > finlandPop);

let avgCountry = 33;
console.log(population > avgCountry);

let description = country + ' is in ' + cont + ' and its ' + population + ' million ' + 'people speak ' + language

console.log(description);

const firstNAme = "Sueda";
const lastNAme = "Akça";
const job = "student";
const birthYear = 2003;
const year = 2023;

const suedaNew = `I'm ${firstNAme}, a ${year - birthYear} years old, ${job}!`;
console.log(suedaNew);

console.log('String \nwith multiple \nlines');

console.log(`String 
with multiple
lines`);

const age = 14;
const isOldEnough = age >= 18;

if (isOldEnough) {
    console.log("Sarah can driving");
}

else {
    const yearsLesft = 18 - age;
    console.log(`Sarah should wait ${yearsLesft} year`);
}

const inputYear = '1991';
console.log(Number(inputYear));
console.log(Number(inputYear) + 18);

console.log('9' - '5', '19' - '13' + '17', '19' - '13' + 17, '123' < 57, 5 + 6 + '4' + 9 - 4 - 2);

const fav = prompt("What is your favorite number? ");
if (fav == 23) {
    console.log("Cool! 23 is an amazing number :)");
}

const numNeighbours = prompt("How many neighbour countries does your country have?");

if (Number(numNeighbours) === 1) {
    console.log("Only 1 border!");
}
else if (numNeighbours > 1) {
    console.log("More then one border!");
}
else {
    console.log("No borders");
}

let isIsland = false;
let country = "Turkey";
let population = 30;
const language = 'english';

if (!isIsland && population < 50 && language == 'english') {
    console.log(`You should live in ${country}:)`);
}

else {
    console.log(`${country} does not meet your criteria :(`);
}

const language = "turkish";
switch (language) {
    case 'spanish':
        console.log("2nd place in number of native speakers");
        break;
    case 'english':
        console.log("3rd place");
        break;
    case 'hindi':
        console.log("Number 4");
    case 'arabic':
        console.log("5th most spoken language");
        break;
    default:
        console.log("Great language too :D");
}
*/
const population = 12;
population > 33 ? console.log("Portugal's population is above average") : console.log("Portugal's population is below average");
