const dScore = (96 + 108 + 89) / 3;
const kScore = (88 + 91 + 110) / 3;

if (dScore > kScore) {
    console.log("Dolphins win the trophy");
}
else if (kScore > dScore) {
    console.log("Koalas win the trophy");
}
else {
    console.log("Both win the trophy");
}