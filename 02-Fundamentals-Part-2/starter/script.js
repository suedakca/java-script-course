'use strict';
/*
function describeCountry(country, population, capitalCity) {
    const final = `${country} has ${population} million people an its capital city is ${capitalCity}`;
    return final;
}

const description = describeCountry('Turkey', 85, 'Ankara');
console.log(description);

const worldPopulation = 7900;

function percentageOfWorld1(population) {
    const percentage = population / worldPopulation * 100;
    return percentage;
}

const populaitonPercentage = percentageOfWorld1(1441);
console.log(Number(populaitonPercentage));


const percentageOfWorld2 = function (population) {
    const percentage = population / worldPopulation * 100;
    return percentage;
}

//Function expression
const calcAge = function (birthYear) {
    return 2023 - birthYear;
}

// Arrow function
const calcAge2 = birthYear => 2023 - birthYear;
const age = calcAge2(2003);
console.log(age);

const yearsRet = birthYear => {
    const age = 2023 - birthYear;
    const retairment = 65 - age;
    return retairment;
}

console.log(yearsRet(2003));

const percentageOfWorld3 = population => population / 7900 * 100;
const percentage = percentageOfWorld3(1441);
console.log(percentage);

const percentageOfWorld3 = population => population / 7900 * 100;
function describePopulation(country, population) {
    const percentage = percentageOfWorld3(population);
    const description = `${country} has ${population} million people, which is about ${percentage} % of the world `
    return description;
}

console.log(describePopulation('China', 1441));

const friends = ['sueda', 'sena', 'ayşe'];
console.log(friends);

const year = new Array(1991, 1992, 1993, 1994, 1995);
console.log(year);

console.log(friends[0], year[3]);
console.log(friends.length);
console.log(friends[friends.length - 3]);
friends[1] = 'zehra';
console.log(friends);

const population = [2300, 4500, 6700, 7800];

console.log(population.length === 4);

const worldPopulation = 7900;

function percentageOfWorld1(population) {
    const percentage = population / worldPopulation * 100;
    return percentage;
}

const percentages = [percentageOfWorld1(population[0]), percentageOfWorld1(population[1]), percentageOfWorld1(population[2]), percentageOfWorld1(population[3])];

console.log(percentages);

const friends = ['sueda', 'zeynep', 'ece'];
const newlength = friends.push('Jay');
console.log(friends);
console.log(newlength);
//new variable can be added in the end of the array with POP
friends.pop();
console.log(friends);
friends.pop();
console.log(friends);
//existed variable can be deleted from end of the arrat with PUSH
friends.push('ahmet');
console.log(friends);
//new variable can be added in the begginning of the array with UNSHIFT
friends.unshift('seda');
console.log(friends);
friends.unshift('sema');
console.log(friends);
//it is check if the variable is exist in the array or not (true/false)
console.log(friends.includes('Steve'));
console.log(friends.includes('sueda'));
//it is check if the variable is exist in the array or not (-1) and show the position of the data

console.log(friends.indexOf('seda'));
console.log(friends.indexOf('bob'));

if (friends.includes('sueda')) {
    console.log('sueda is queen');
}

// object structure - similar with strcut in c++
const sueda = {
    firstName: 'sueda',
    lastName: 'akça',
    age: 2023 - 2003,
    job: 'student',
    friends: ['sueda', 'zeynep', 'ece']
};
console.log(sueda);
console.log(sueda.firstName);
console.log(sueda['job']);

const nameKey = 'Name';
console.log(sueda['first' + nameKey]);
console.log(sueda['last' + nameKey]);

const insterestedIn = prompt('what do you want to know about sueda? Choose between firstName, lastName, age, job and friends');
if (sueda[insterestedIn]) {
    console.log(sueda[insterestedIn]);
}
else {
    console.log('Wrong request');
}

sueda.location = 'Turkey';
sueda['Twitter'] = '@suceda';
console.log(sueda);

console.log('Sueda has ' + sueda.friends.length + ' friends, and his best friends is called ' + sueda.friends[1]);

console.log(`Sueda has ${sueda.friends.length} friends, and her best friend called ${sueda.friends[1]}`);

const neighbours = ['england', 'usa', 'sweden'];
neighbours.push('utopia');
neighbours.pop();
if (neighbours.includes('germany')) {
    console.log('I might be know your country :D');
}
else {
    console.log('Probably not a central European country :D');
}
let pos = neighbours.indexOf('sweden');
neighbours[pos] = 'republic of sweden';
console.log(neighbours);

const myCountry = {
    country: 'Turkey',
    capital: 'Ankara',
    language: 'Turkish',
    populaiton: 85,
    neighbours: ['england', 'usa', 'sweden']
};

console.log(myCountry);

console.log(`${myCountry.country} has ${myCountry.populaiton} million ${myCountry.language}-speaking people, ${myCountry.neighbours.length} neighbours countries and a capital called ${myCountry.capital}`);
*/

const sueda = {
    firstName: 'sueda',
    lastName: 'akça',
    age: 2023 - 2003,
    birthYear: 2003,
    job: 'student',
    friends: ['sueda', 'zeynep', 'ece'],
    hasDriverLicense: true,
    calcAge: function () {
        return 2023 - this.birthYear;
    },
    getSummary: function () {
        return `${this.firstName} is a ${this.calcAge()}-years old ${this.job}, and he has ${this.hasDriverLicense ? 'a' : 'no'} driver's license`
    }
};

console.log(sueda);
console.log(sueda.calcAge(2003));
console.log(sueda['calcAge'](2003));

console.log(sueda.getSummary());